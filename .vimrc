set nu ts=4 sw=4 ai mouse=a

set ruler

filetype plugin on
colorscheme molokai
set t_Co=256
syntax on

execute pathogen#infect()
set guifont=DejaVu\ Sans\ mono\ 11

map <C-c> <ESC>:!g++ % -o %< -Wall -O2 -std=c++11<CR>
